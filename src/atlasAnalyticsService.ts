import { getEntityWorldPosition, Interval } from "@dcl/ecs-scene-utils";
import {
	getCurrentRealm,
	getPlatform,
	Platform,
} from "@decentraland/EnvironmentAPI";
import { getUserData } from "@decentraland/Identity";
import { signedFetch } from "@decentraland/SignedFetch";
import { getParcel } from "@decentraland/ParcelIdentity";

@Component("atlasAnalyticsFlag")
export class AtlasAnalyticsFlag {}

/**
 * Log Levels: TRACE < DEBUG < INFO < WARN < ERROR < FATAL
 */

type LogLevel = "FATAL" | "ERROR" | "WARN" | "INFO" | "DEBUG" | "TRACE";

interface EventData {
	eventName: string;
	player: string;
	guest: boolean;
	playerPosition: Vector3;
	playerRotation: Vector3;
	realm: string | undefined;
	timestamp: number;
	data?: OptionalData;
	sceneName?: string;
	sceneBranch?: string;
}

interface OptionalData {
	buttonPosition?: Vector3;
	cameraState?: number;
	expression?: string;
	clickedPlayer?: string;
	locked?: boolean;
	idle?: boolean;
	version?: number;
	videoData?: VideoData;
	sceneInitData?: SceneInitData;
	playerName?: string | undefined;
	birthday?: Birthday;
	logData?: LogData;
}

interface LogData {
	error?: string;
	level?: LogLevel;
}

interface Birthday {
	month: number;
	day: number;
	year: number;
}

interface SceneInitData {
	platform: Platform;
	startTime: number;
	endTime: number;
	analyticsVersion: string;
	parcels: string[];
	tags: string[];
	pollingInterval: number;
	description?: string;
	navmapThumnail?: string;
	favicon?: string;
}

interface VideoData {
	videoClipId: string;
	componentId: string;
	currentOffset: number;
	totalVideoLength: number;
	videoStatus: number;
}

export class AtlasAnalyticsService extends Entity {
	pingPeriod: number;
	shape?: GLTFShape;
	apiURL: string;
	timeDelayEntity!: Entity;
	startTime: Date;
	endTime!: Date;
	debug: boolean;

	analyticsVersion = "0.4.8";

	sceneName?: string;
	sceneBranch: string;
	player!: string;
	playerName!: string;
	guest!: boolean;
	platform!: Platform;
	realm?: string;
	parcels!: string[];
	tags!: string[];
	description?: string;
	navmapThumbnail?: string;
	favicon?: string;

	constructor(sceneBranch: string = "Default") {
		super();
		this.startTime = new Date();
		this.pingPeriod = 5000;
		void this.initializePlayerData();
		this.sceneBranch = sceneBranch;
		void this.enableSceneTimer();
		this.addComponent(new AtlasAnalyticsFlag());

		this.debug = true;

		this.apiURL = "https://analytics.atlascorp.io/";
		// this.apiURL = "http://localhost:8080/check-validity";

		this.timeDelayEntity = new Entity();
		engine.addEntity(this.timeDelayEntity);

		this.initializeAnalytics();
	}

	private async initializePlayerData() {
		await this.updateUserData();
		await this.updateSceneMetadata();

		this.platform = await getPlatform();

		const playerRealm = await getCurrentRealm();
		this.realm = playerRealm?.displayName;
	}

	public updateBranchName(name: string) {
		this.sceneBranch = name;
	}

	private async updateSceneMetadata() {
		const parcel = await getParcel();
		this.sceneName = parcel.land.sceneJsonData.display?.title;
		this.parcels = parcel.land.sceneJsonData.scene.parcels;
		this.tags = parcel.land.sceneJsonData.tags!;
		this.description = parcel.land.sceneJsonData.display?.description;
		this.navmapThumbnail =
			parcel.land.sceneJsonData.display?.navmapThumbnail;
		this.favicon = parcel.land.sceneJsonData.display?.favicon;
	}

	private async updateUserData() {
		const userData = await getUserData();
		log(userData);
		this.player = userData!.userId;
		this.guest = !userData?.hasConnectedWeb3;
		this.playerName = userData!.displayName;
	}

	private initializeAnalytics() {
		// if (this.pingPeriod !== 0 && this.pingPeriod >= 2000) {
		// 	void this.enablePing(this.pingPeriod);
		// } else if (this.debug) {
		// 	log("Ping Disabled:", this.pingPeriod);
		// }
		// void this.enableRealmChange();
		// void this.enableEntersOrLeavesScene();
		// void this.enablePlayerChangesCameraMode();
		// void this.enablePlayerAnimation();
		// void this.enablePlayerClickPlayer();
		// void this.enablePlayerLocked();
		// void this.enableIdle();
		// void this.enableProfileChange();
	}

	public async submitButtonEvent(name: string, button: Entity) {
		const buttonPosition = getEntityWorldPosition(button);
		const postBody = await this.getPostBody(name, {
			buttonPosition: buttonPosition,
		});

		if (this.debug) log(postBody);
		void this.submitSignedFetch(postBody);
	}

	public async submitErrorLog(error: string, level: LogLevel) {
		const postBody = await this.getPostBody("error-log", {
			logData: { error, level },
		});

		if (this.debug) log(postBody);
		void this.submitSignedFetch(postBody);
	}

	public async submitBirthdayEvent(name: string, birthday: Birthday) {
		const postBody = await this.getPostBody(name, {
			birthday: birthday,
		});

		if (this.debug) log(postBody);
		void this.submitSignedFetch(postBody);
	}

	public async submitGenericEvent(name: string) {
		const postBody = await this.getPostBody(name, {});
		if (this.debug) log(postBody);
		void this.submitSignedFetch(postBody);
	}

	public async enablePing(interval: number) {
		this.timeDelayEntity.addComponent(
			new Interval(interval, async () => {
				const postBody = await this.getPostBody("ping");
				if (this.debug) log("yeet", postBody);
				void this.submitSignedFetch(postBody);
			})
		);
	}

	// Event handler called for every scene entrance and exit event.
	// Needs to be filtered to Players address
	public async enableEntersOrLeavesScene() {
		onEnterSceneObservable.add(async (player) => {
			const myPlayer = await getUserData();

			if (myPlayer?.userId === player.userId) {
				const postBody = await this.getPostBody("enters-scene");
				void this.submitSignedFetch(postBody);
				if (this.debug) log(postBody);
			}
		});

		onLeaveSceneObservable.add(async (player) => {
			const myPlayer = await getUserData();
			if (myPlayer?.userId === player.userId) {
				const postBody = await this.getPostBody("leaves-scene");
				void this.submitSignedFetch(postBody);
				if (this.debug) log(postBody);
			}
		});
	}

	// Event handler only called for players avatar
	public async enablePlayerChangesCameraMode() {
		onCameraModeChangedObservable.add(async ({ cameraMode }) => {
			const postBody = await this.getPostBody("camera-change", {
				cameraState: cameraMode,
			});
			void this.submitSignedFetch(postBody);
			if (this.debug) log(postBody);
		});
	}

	// Event handler only called for players avatar
	public async enablePlayerAnimation() {
		onPlayerExpressionObservable.add(async ({ expressionId }) => {
			const postBody = await this.getPostBody("player-animation", {
				expression: expressionId,
			});
			void this.submitSignedFetch(postBody);
			if (this.debug) log(postBody);
		});
	}

	// Event handler only called for players avatar
	public async enablePlayerClickPlayer() {
		onPlayerClickedObservable.add(async (clickEvent) => {
			const postBody = await this.getPostBody("player-click", {
				clickedPlayer: clickEvent.userId,
			});
			void this.submitSignedFetch(postBody);
			if (this.debug) log(postBody);
		});
	}

	// Event handler only called for players avatar
	public async enablePlayerLocked() {
		onPointerLockedStateChange.add(async ({ locked }) => {
			const postBody = await this.getPostBody("locked-view", {
				locked: locked!,
			});
			void this.submitSignedFetch(postBody);
			if (this.debug) log(postBody);
		});
	}

	// Event handler only called for players avatar
	// Events only trigger if the user has the browser tab active
	public async enableIdle() {
		onIdleStateChangedObservable.add(async ({ isIdle }) => {
			const postBody = await this.getPostBody("idle", { idle: isIdle });
			void this.submitSignedFetch(postBody);
			if (this.debug) log(postBody);
		});
	}

	public async enableProfileChange() {
		onProfileChanged.add(async (profileData) => {
			await this.updateUserData();
			const postBody = await this.getPostBody("profile-change", {
				version: profileData.version,
				playerName: this.playerName,
			});
			void this.submitSignedFetch(postBody);
			if (this.debug) log(postBody);
		});
	}

	public async enableSceneTimer() {
		onSceneReadyObservable.add(async () => {
			this.endTime = new Date();

			const postBody = await this.getPostBody("load-timer", {
				playerName: this.playerName,
				sceneInitData: {
					platform: this.platform,
					startTime: this.startTime.getTime(),
					endTime: this.endTime.getTime(),
					analyticsVersion: this.analyticsVersion,
					parcels: this.parcels,
					tags: this.tags,
					pollingInterval: this.pingPeriod,
					description: this.description,
					navmapThumnail: this.navmapThumbnail,
					favicon: this.favicon,
				},
			});

			void this.submitSignedFetch(postBody);

			if (this.debug) log(postBody);
		});
	}

	public async enableVideo() {
		onVideoEvent.add(async (data) => {
			const postBody = await this.getPostBody("video", {
				videoData: data,
			});
			void this.submitSignedFetch(postBody);
			log(postBody);
		});
	}

	public async enableRealmChange() {
		onRealmChangedObservable.add(async (realmChange) => {
			this.realm = realmChange.displayName;
		});
	}

	private async assemblePlayerData() {
		const playerWorldPosition = Camera.instance.worldPosition;
		const playerRotation = Camera.instance.rotation.eulerAngles;

		return {
			player: this.player,
			guest: this.guest,
			playerPosition: playerWorldPosition,
			playerRotation: playerRotation,
			realm: this.realm,
			sceneName: this.sceneName,
			sceneBranch: this.sceneBranch,
			timestamp: new Date().getTime(),
		};
	}

	private async getPostBody(
		eventName: string,
		data?: OptionalData
	): Promise<EventData> {
		const result = await this.assemblePlayerData();

		return {
			...result,
			eventName: eventName,
			data,
		};
	}

	private async submitSignedFetch(postBody: EventData) {
		try {
			const response = await signedFetch(this.apiURL, {
				headers: {
					"Content-Type": "application/json",
					"User-Agent": "AtlasCorp",
					"Cache-Control": "no-cache",
				},
				method: "POST",
				body: JSON.stringify(postBody),
			});
			// let json = await response.json();
			if (this.debug) log(response);
		} catch {
			if (this.debug) log("failed to reach analytics URL");
		}
	}

	public logAndStore = async (message: string, level: LogLevel) => {
		log(message);
		void this.submitErrorLog(message, level);
	};
}

/************************
* 		Change Log		*
*************************

0.4.8

- Removed branch name, polling interval, and debug constants
- Removed class instantiation and export
- Fixed polling rate to 5000 ms
- Fixed debug to true
- Fixed analytics version to 0.4.8

0.4.7

- Fixes spelling error in analytics version variable name
- Adds comments about each event trigger

0.4.6

- Updates all trivially inferred types
- Addresses all promises that are not awaited

0.4.5

- Adds wrapper for log() with error levels to save to server and print to console.

0.4.4

- Added description, navmapthumbnail, and favicon to the scene init data packet

0.4.3

- Added function to change scene branch name after analtyics entity is created
- Addded AtlasAnalyticsFlag custom component to identify entity in cases of scene wiping for instancing
- Updated verison number
- Added comments to 3 branch name, polling interval, and debug  
*/
